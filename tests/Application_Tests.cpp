#include <Simplex/Test.h>
#include <Simplex/GLFW/Application.h>

TEST ( SimplexGlfwApplication, InheritsFromSimplexCoreApplication )
{
  Simplex::GLFW::Application app = Simplex::GLFW::Application ();
  
  ASSERT_TRUE( static_cast< Simplex::Core::Application* >( &app ) );
}
