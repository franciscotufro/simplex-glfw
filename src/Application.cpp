#include <Simplex/GLFW/Application.h>
#include <Simplex/Core/Exception.h>
#include <Simplex/Graphics/GraphicsApiAdapter.h>
#include <Simplex/OpenGL/GraphicsApiAdaptee.h>


static void SimplexGLFWErrorCallback ( int error, const char* description )
{
  Simplex::Core::Exception e = Simplex::Core::Exception ( description );
  Simplex::Core::Exception::Throw ( e );
}

static void SimplexGLFWKeyCallback ( GLFWwindow* window, int key, int scancode, int action, int mods )
{
    if ( key == GLFW_KEY_ESCAPE && action == GLFW_PRESS )
        glfwSetWindowShouldClose ( window, GL_TRUE );
}

namespace Simplex
{

  namespace GLFW
  {
    Application::Application ()
    {
      if( !glfwInit () )
      {
        Simplex::Core::Exception e = Simplex::Core::Exception ( "Cannot initialize GLFW." );
        Simplex::Core::Exception::Throw ( e );
      }

      glfwSetErrorCallback ( SimplexGLFWErrorCallback );
    }

    Application::~Application ()
    {
      glfwTerminate ();
    }

    void Application::OpenWindow ( int width, int height, std::string title, bool fullscreen )
    {
      GLFWmonitor* monitor = fullscreen ? glfwGetPrimaryMonitor () : NULL;

      glfwWindowHint ( GLFW_CLIENT_API, GLFW_OPENGL_API );
      glfwWindowHint ( GLFW_CONTEXT_VERSION_MAJOR, 2 );
      glfwWindowHint ( GLFW_CONTEXT_VERSION_MINOR, 1 );
      // glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

      mWindow = glfwCreateWindow ( width, height, title.c_str (), monitor, NULL );

      if ( !mWindow )
      {
        glfwTerminate ();
        Simplex::Core::Exception e = Simplex::Core::Exception ( "Window cannot be created." );
        Simplex::Core::Exception::Throw ( e );
      }
    
      glfwMakeContextCurrent ( mWindow );

      // Initialize GLEW
      if ( glewInit () != GLEW_OK ) {
        Simplex::Core::Exception e = Simplex::Core::Exception ( "Failed to initialize GLEW." );
        Simplex::Core::Exception::Throw ( e );
      }


      glfwSetKeyCallback ( mWindow, SimplexGLFWKeyCallback );

      Simplex::OpenGL::GraphicsApiAdaptee* adaptee = new Simplex::OpenGL::GraphicsApiAdaptee ();
      Simplex::Graphics::GraphicsApiAdapter::Instance ()->SetAdaptee ( adaptee );
      Simplex::Graphics::GraphicsApiAdapter::Instance ()->Initialize ();

    }

    void Application::CloseWindow ()
    {
      Simplex::Graphics::GraphicsApiAdapter::Instance ()->Shutdown ();
      glfwDestroyWindow ( mWindow );
    }

    void Application::Start ()
    {
      mSceneGraph->Start ();

      while ( !glfwWindowShouldClose ( mWindow ) )
      {
        float ratio;
        int width, height;
        glfwGetFramebufferSize ( mWindow, &width, &height );

        glViewport ( 0, 0, width, height );
        glClear ( GL_COLOR_BUFFER_BIT );

        mSceneGraph->Update ();
        mSceneGraph->Render ();

        glfwSwapBuffers ( mWindow );
        glfwPollEvents ();
      }
    }

  }
}
