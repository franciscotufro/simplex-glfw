#ifndef __SIMPLEX_GLFW_APPLICATION_H__
#define __SIMPLEX_GLFW_APPLICATION_H__

#include <Simplex/OpenGL/third-party.h>

#include <GLFW/glfw3.h>

#include <Simplex/Core/Application.h>

namespace Simplex
{
  namespace GLFW
  {
    class Application : public Simplex::Core::Application
    {
      
    public:
      Application ();
      ~Application ();
      
      void OpenWindow (int width, int height, std::string title, bool fullscreen);
      void CloseWindow ();

      void Start ();
    
    private:
      GLFWwindow* mWindow;

      void Render ();
      void Update ();

    };  
  }
}


#endif