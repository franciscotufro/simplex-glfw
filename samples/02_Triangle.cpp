#include <Simplex/GLFW/Application.h>
#include <Simplex/Graphics/Renderable.h>
#include <Simplex/Graphics/ShaderFactory.h>
using namespace Simplex::Graphics;

int main( int argc, char** argv) 
{
  // Open Application
  Simplex::GLFW::Application* app = new Simplex::GLFW::Application();
  app->OpenWindow (640, 480, "02_Triangle", false);
	
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

  Renderable* t = new Renderable ();
  t->SetName ( "Triangle" );
  
  // Create Mesh
	Vector3 vertices[3] = { Vector3(-1.0f, -1.0f, 0.0f), Vector3(1.0f, -1.0f, 0.0f), Vector3(0.0f,  1.0f, 0.0f) };
	Mesh* mesh = new Mesh ( 3, vertices );
	t->SetMesh ( mesh );

	// Register Shaders
	Simplex::Graphics::ShaderDefinition shaders[2] = 
	{
		{ "02_TriangleVertex", "./Shaders/02_Triangle.vert", Simplex::Graphics::ShaderType::VERTEX },
		{ "02_TriangleFragment", "./Shaders/02_Triangle.frag", Simplex::Graphics::ShaderType::FRAGMENT },
	};

	ShaderFactory::Instance()->Define ( shaders[0] );
	ShaderFactory::Instance()->Define ( shaders[1] );

	Shader* vertex = ShaderFactory::Instance()->Create ( "02_TriangleVertex" );
	Shader* fragment = ShaderFactory::Instance()->Create ( "02_TriangleFragment" );

	// Create Material
	Material* mat = new Material ();
	mat->AttachShader ( vertex );
	mat->AttachShader ( fragment );

	t->SetMaterial ( mat );

  app->GetSceneGraph()->AddSceneObject ( t );

  app->Start ();
  
  delete t;
  delete mesh;
  delete mat;
  delete vertex;
  delete fragment;
  delete app;
}