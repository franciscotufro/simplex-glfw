#include <Simplex/GLFW/Application.h>

int main( int argc, char** argv) 
{
  // Open Application
  Simplex::GLFW::Application* app = new Simplex::GLFW::Application();
  app->OpenWindow (640, 480, "01_OpenWindow", false);
  app->Start ();
  delete app;
}