cmake_minimum_required(VERSION 2.8.5)
project(simplex-glfw)

add_subdirectory ( third-party/glfw )

file ( GLOB_RECURSE CODE
 ${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp
 ${CMAKE_CURRENT_SOURCE_DIR}/include/*.h
)

file ( GLOB TESTS_CODE
 ${CMAKE_CURRENT_SOURCE_DIR}/tests/*
)

#file ( GLOB QUAD_CODE
# ${CMAKE_CURRENT_SOURCE_DIR}/samples/Quad.cpp
#)

add_library ( simplex-glfw STATIC 
  ${CODE}
)

include_directories ( ${CMAKE_CURRENT_SOURCE_DIR}/third-party/glfw/include )

set ( SAMPLES
	01_OpenWindow
	02_Triangle
)

foreach(SAMPLE ${SAMPLES})
	
	add_executable ( ${SAMPLE} ${CMAKE_CURRENT_SOURCE_DIR}/samples/${SAMPLE}.cpp )
	target_link_libraries ( ${SAMPLE} simplex-glfw simplex-opengl simplex-graphics simplex-core simplex-math glfw ${GLFW_LIBRARIES} ${GRAPHICS_LIBRARIES} )

endforeach(SAMPLE)

add_custom_command(TARGET 02_Triangle PRE_BUILD
	COMMAND ${CMAKE_COMMAND} -E copy_directory
  ${CMAKE_CURRENT_SOURCE_DIR}/samples/Shaders $<TARGET_FILE_DIR:02_Triangle>/Shaders)

add_executable ( SimplexGlfwTests ${TESTS_CODE} )
target_link_libraries ( SimplexGlfwTests simplex-glfw simplex-opengl simplex-graphics simplex-core simplex-math glfw ${GLFW_LIBRARIES} ${TEST_LIBRARIES} ${GRAPHICS_LIBRARIES})
add_test ( SimplexGlfwTests SimplexGlfwTests )
